var mongoose = require('mongoose');
const User = require('../../models/user.js');

exports.validateInput = validateInput;
exports.createUser = createUser;
exports.response = response;

mongoose.connect('mongodb://localhost:27017/gighunter');

function validateInput(req, res, next) {  
  var newUser = User(req.body.newUser);
  req.newUser = newUser;
  next();
}

function createUser(req, res, next) {
  // save the user
  req.newUser.save(function(err) {
    if (err) throw err;
  });

  next();
}

function response(req, res, next) {
  res.send('User created!');
}
