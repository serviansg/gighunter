var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: false },
    password: { type: String, required: true, select: false },
    email: { type: String, required: true, unique: true },
    createdAt: {type: Date, default: Date.now },
    updatedAt: Date
});

var User = mongoose.model('User', userSchema);

module.exports = User;
