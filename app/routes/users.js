var express = require('express');
var router = express.Router();

const signupController = require('../controllers/users/signup.controller.js');

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

/* POST users - Sign up. */
router.post('/', signupController.validateInput, signupController.createUser, signupController.response);

/* POST login - user login. */
router.post('/login', function (req, res, next) {
  res.send('respond with a resource');
});
module.exports = router;
